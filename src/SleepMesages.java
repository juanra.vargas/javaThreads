

public class SleepMesages{
    
    public static void main(String args[]) throws InterruptedException{
        String ImportantInfo[] = {
            "Mares eat oats",
            "Does eat oats",
            "Little lambs eat ivy",
            "A kid will eat ivy too"
        };
        
        for (int i = 0; i < ImportantInfo.length; i++) {
            ///Pause for 4 seconds 
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //We've been interrupted: No more messages 
                return;
            }
            //prints a message
            System.out.println(ImportantInfo[i]);
        }

    }

}