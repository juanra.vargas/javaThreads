/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javathreads;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Vargas
 */
public class JavaThreads extends Thread{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //(new Thread(new JavaThreads())).start();
        (new JavaThreads()).start();
    }

    //@Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("\n\nHello from a thread\n\n");
        Integer i = 0, j = 0, k = 0;
        do{
            j++;
            k++;
            Integer P = j + k;
            
            try {
                Thread.sleep(1000);
                System.out.println(P);
            } catch (InterruptedException ex) {
                Logger.getLogger(JavaThreads.class.getName()).log(Level.SEVERE, null, ex);
            }
            i++;
        }while(i < 20);
    }
    
}
